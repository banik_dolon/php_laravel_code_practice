<?php
//Array
    $people = array("A", "B", "C");
    print_r($people); //For full array output
    echo $people[2];  //For particular array output
    echo "\n";

    $numbers = array(10, 20, 90);
    $sum = 500;
    foreach ($numbers as $number){
        $sum = $sum + $number;
    }
    echo $sum;
    echo "\n";


    $movie[0] = "Shaolin Monk";
    $movie[1] = "Drunken Master";
    $movie[2] = "American Ninja";
    $movie[3] = "Once upon a time in China";
    $movie[4] = "Replacement Killers";
    echo $movie[3];
    $movie[3] = " Eastern Condors";
    echo $movie[3];

    echo "\n";
    //OR,

    $movie = array(0 => "Shaolin Monk",
        1 => "Drunken Master",
        2 => "American Ninja",
        3 => "Once upon a time in China",
        4 => "Replacement Killers");
    echo $movie[4];
    echo "\n";


    $persons = array("Mary" => "Female", "John" => "Male", "Mirriam" => "Female");
    print_r($persons);
    echo "Mary is a " . $persons["Mary"];
    echo "\n";

    //***Multidimensional array

    $movies = array(
        "comedy" => array("Pink Panther", "John English", "See no evil hear no evil"),
        "action" => array("Die Hard", "Expendables"),
        "epic" => array("The Lord of the rings"),
        "Romance" => array("Romeo and Juliet")
    );
    print_r($movies);  //To print properly an array > you either loop through it and echo each element, or you can use print_r.
    echo "\n";

    //***Count function
    $lecturers = array("Mr. Jones", "Mr. Banda", "Mrs. Smith", "Mr Halou");
    echo count($lecturers);
    echo "\n";

    //***Sort
    $persons = array(0 => "Male", 1 => "Female", 2 => "Female");
    sort($persons);
    print_r($persons);




